#!/bin/bash
echo " [...] Checking internet connection [...] " 
ping -c3 8.8.8.8 >> /dev/null #on envoie le ping dans un autre fichier pour le laisser cacher
TEST=$? #on recupere le ping dans la variable test

if [ $TEST -ne 0 ] #si le ping n'est pas égal à 0
	then	
		echo " [/!\] Not connected to internet [/!\] "
		echo " [/!\] Please check your configuration [/!\] "
	else		
		echo " [...] Internet access OK [...] "
fi


